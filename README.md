# TBo-CLI

This is a [termux](https://wiki.termux.com/wiki) [borg](https://borgbackup.readthedocs.io/en/stable/index.html) client script for automated back ups from android devices. It is especially aimed at those who already back up some of their devices with borg and wish not to use a different solution for their mobile devices.

Using this script, you can back up arbitrary directories and files to your borg backup server(s).

This script is suited best for you if you

* already have borg server you have access to.
* have android devices that are not (sufficiently) back uped (yet).
* are fine with installing apps from the [fdroid store](https://f-droid.org/), which is a free and open source alternative to the Google Play Store.
* have a rough idea how to use bash.

There is no need to have root access to your device. (Except if you want to back up directories the normal user has no access to.)

## Setup

### A quick note on terminology

This documentation refers to any device to create backups from as "the client", and any device that accepts backups as "the server".

### Dependencies (on the client devices)

1. Install the [termux app](https://github.com/termux/termux-app) from the [fdroid store](https://f-droid.org/packages/com.termux/) (recommended) or the [android store](https://play.google.com/store/apps/details?id=com.termux&hl=en_GB) (deprecated and discouraged). When opening the app, you will see a traditional linux command line interface (CLI). [By default](https://wiki.termux.com/wiki/Shells) this is a bash shell. The recommended package manager to use is [`pkg`](https://wiki.termux.com/wiki/Package_Management).
2. Install the [termux:API app](https://github.com/termux/termux-api). This allows termux to show notifications. (on [fdroid](https://f-droid.org/packages/com.termux.api/), in the [play store](https://play.google.com/store/apps/details?id=com.termux.api))
3. This projects needs some permissions. In order to grant them, you can either allow the app `termux` in your devices settings to "access files on your device" and to "allow background activity", or you trigger the request in the app once:
    * Permission for storage: Run `termux-setup-storage` and allow the requested permission (not just once, in case there are multiple options).
    * Permission for background acitivity: Run `termux-wake-lock` and allow the requested permission (not just once, in case there are multiple options). Run `termux-wake-unlock` afterwards, as background-acitivity should only be turned on when necessary.
4. The program depends on some other packages:
    * `openssh` for key generation (is only done manually during the setup process)
    * `termux-api` for most device commands (the `termux-*` commands), e.g. for checking the battery status or sending notifications.
    * `borgbackup` for running the actual backup
    * at some point, you will need an editor. This is a convenient time to install your command line editor of choice, like `vim` or `nano`.
    * `git` for copying this repository to your device (technically not needed if you prefer to copy the scripts by other means)

    Run first `pkg upgrade` and then `pkg install <package>` for each of the packages.


### Client setup (once)

This assmues a single client. I you have multiple, repeat this step for every single one.

1. Run `git clone https://gitlab.com/blitter/tbo-cli.git`
2. Choose a hostname for the device. As the android device name is not the hostname and [linux hostname can't be changed without root](https://android.stackexchange.com/a/174520) this is independent from the device name chosen in the Android settings. Choose any string without whitespace and write it to the file `config/hostname`.
3. If there is no ssh key on the client yet (`~/.ssh/id_<keytype>`), create one. For example use the command `ssh-keygen -t ed25519`. Now you have a key pair.
4. Find out what you want to backup (for example local chat backups, some settings, your pictures, etc)
5. Write your [client's configuration](#client-configuration).


### Client Setup (once per server)

If this server was never used before, do these preparatory steps first:

1. If this is a server never used before: pick a name for how to refer to the server. No whitespaces.
2. Create a file `config/hosts/<servername>.sh`. Fill the settings in as described in the [server configuration section](#server-configuration).


For each server, this client should backup to, do these steps:

1. Put the clients ssh public key into `~/.ssh/authorized_keys` on the server ([explanation](https://www.howtouselinux.com/post/ssh-authorized_keys-file))
2. Create a borg repository on the server ([borg init docs](https://borgbackup.readthedocs.io/en/stable/usage/init.html)). Follow the naming scheme used in this project: `<basedir>/<hostname>` where `basedir` is the directory all borg backups created by TBo-CLI should go and `hostname` is the specific clients hostname
    1. Pick a password for the repository. (I recommend [diceware](https://theworld.com/~reinhold/diceware.html). There are packages in most Linux distributions, e.g. on [Debian](https://packages.debian.org/stable/utils/diceware), [Ubuntu](https://packages.ubuntu.com/focal/diceware),  [NixOS](https://search.nixos.org/packages?channel=unstable&show=diceware), and there is [a python package](https://github.com/ulif/diceware/#install))
    2. Keep the password at a safe place in case you need to restore from the backup! (and the keyfile, if you chose the encryption method `keyfile`)
    3. Put the password to the file `secrets/<servername>`


## How to write your config

These files should exist in your config folder.

```
config
|- hostname
|- hosts
|   |- my-favourite-server.sh
|   |- ...
|- clients
|   |- phone
|   |- tablet
|   |- ...
```

Of course, this is not how you should name devices. [How (not) to name devices.](https://datatracker.ietf.org/doc/html/rfc1178)

* The `hostname` file should contain the string how to refer to the specific client this file is on (the `client-name` of the specific device). If you use version control for your configuration, this is the only file that has to be in the `.gitignore`.
* The files in `hosts` are the server settings (`<servern-name>.sh`)
* The files in `clients` are the client settings (`<client-name>`)

### Client Configuration

This file should contain a new-line separated list of the directories on the device that shall be backuped.

* all directories need to be accessible to the termux shell on the device
* whitespace has to be escaped


### Server Configuration

```sh
#!/data/data/com.termux/files/usr/bin/bash

HOST="127.0.0.1" # the server's public IP address
PORT="5000" # the port on which ssh server runs (default 22)
USERNAME="me" # your ssh user on that server
BASEDIR="/./my-repo-dir" # directory to the folder where all backups belong
```

The `basedir` should be an absolute URL beginning with a `/`, but not having a trailing `/`. A typical example would be: `/./repository`.


### Secrets

There are multiple types of secrets:

* the client's ssh privatekey
* the client's repokeys (one per server) in `secrets/<server-name>`

Make sure to put neither into any type of version control system.

It's a good idea to use one ssh key per device: In case one device gets compromised, only its key has to be disabled. As the clients devices will never be your only means to log onto the server they do not need to be stored elsewhere.

The repository keys are the only way to access the repository. In order to be able to recover from your back ups, keep a copy of them at a safe location (for example your password manager).


### Distributing the Configuration

The general idea is to put the `configuration` on all devices. There are multiple options to do so, even using git. Here are 2 of them:

1. Create a git repository containing your configuration. Create a soft [symbolic link](https://www.baeldung.com/linux/soft-links-to-directories) from `tbo-cli/configuration` to the configuration repository. It's up to you whether you put the secrets in the main repo (does not require a symbolic link), your config repo or a different location (requires a symbolic link)
2. Use `git submodule`. The configuration will adapt according to this repository, not the other way around. Therefore this repository has to be a submodule of your configuration:

    ```
    your-config
    |- config/ (this folder contains all the configuration files)
    |- secrets/ (this folder contains all the secrets)
    |- src/ (location of this repository, added as a submodule)
    ```

    Create a symbolic link for both `config` and `secrets`. I recommend using a small setup script (that you could put into your config repo) for doing so:

    ```sh
    ln -sr config/ src/config
    ln -sr secrets/ src/secrets
    ```

    Don't forgot to pull the submodule. Clone and pull with the option `--recurse-submodules`. Update the submodule from time to time (and update your config accordingly).

Here is an example `.gitignore` for your repository:

```
secrets
config/hostname
.kate-swp
```

(Don't forgot to ignore whatever intermediate files your editor produces.)

Either way, you might want to restart the (regurlarly running `termux-scheduler`) job whenever you pulled some changes. For now, I recommend cancelling the old job manually and having a `restart.sh` script ready, containing the command described in [the section on the regurlarly running job](#regurlarly-running-the-backup-job).

If you are still reading this, and you wonder why this is so complicated – so do I. Please reach out if you have a better idea.


### Script Execution

Run

```sh
bash backup.sh
```

in the termux shell from the directory of this repository.

### Arguments

`--BORGBACKUP_FORCE 1` (the value `1` can be replaced by any except `0`) attempts to execute backup even though the constraints are not fulfilled.

### On Nightly Versions

On the branch [`nightly`](https://gitlab.com/blitter/tbo-cli/-/tree/nightly) the features for the next version will slowly be added. Yes, that includes breaking changes.


## A Prune is not a Compact

For borg, a `prune` is "marking a backup as 'do not keep'". A `compact` is actually reorganizing the backups / freeing up disk space. You might want to run `compact` from time to time.

## Regularly running the backup job

Here is an example scheduled job to run on the server for running a compact once a week. First, mark the `backup.sh` as executable using `chmod u+x backups.sh`. Then start the job:

```sh
termux-job-scheduler \
  --script <absolute-dir-to-backup.sh> \
  --period-ms 1800000 \
  --network unmetered \
  --persisted true
```

The directory has to be the absolute path. (May begin with `~`.)


You can create a `restart.sh` doing these steps automated. Execute it using `bash restart.sh`.

```sh
BACKUP_SCRIPT=~/production/tbo-config/src/backup.sh

chmod u+x $BACKUP_SCRIPT

termux-job-scheduler \
  --script $BACKUP_SCRIPT \
  --period-ms 1800000 \
  --network unmetered \
  --persisted true
```


How to see if a cron job is currently running: (shows jobs and their ids)

```sh
termux-job-scheduler -p
```

How to stop the job:

```sh
termux-job-scheduler --cancel --job-id <id>
# or
termux-job-scheduler --cancel-all
```

# Troubleshooting

## The script blocks

Check if `termux-notification` blocks the shell. If so, check if the `termux:API` fdroid app and the `termux-api` termux pkg are installed.


# Dev Docs

This is the documentation of the internal design of the backup script.

This is a `bash` script (instead of, for example, a posix shell script or a program in one of the tpyical programming languages). Why?

* `termux` is a bash shell. So this does not introduce new dependencies
* the `termux-*` commands (that show information about the devices status) are shell commands
* there was a good example I could base this script on

Will this always stay a bash script, or will there be a rewrite in a programming language? A definitive Maybe.


## Program structure

1. check general constraints (e.g. battery status).
2. For each server for which a borg repository key is known:
    1. Check repository contraints (e.g. ability to ping it)
    2. back up
    3. prune

When the general constraints aren't fulfilled and the script is executed without force mode, quit the whole program. When a servers constraints fail (and the script is not executed in force mode) skip this server.

When the script is executed in force mode, try to back up nonetheless.


## Assumptions of  the script (as for version 1)

This very first version of the TBo-CLI makes some assumptions that cannot be changed trivially / from a config file. The assumptions this script makes are:

* The servers are (only) available through an ssh connection.
* The servers accepts publickeys.
* You are able to configuration additional publickeys accepted by the servers.
* There should be only made one backup per day.
* Backing up should only be attempted when the device is connected to a Wifi, but it does not differentiate between multiple Wifis.
* Backup up to a specific server should only be attempted when able to ping that server.
* Backing up should only be attempted when the phone is charging and over 10% of battery or has at least 80% of battery.
* All backups from the last 14 days shall be kept, daily backups from the last 31 days, weekly backups from the last 24 weeks (~ half a year), monthly backups for 2 years, and one backup for the last 10 years.

These assumptions can only be changed by manipulating the script. Some of these assumptions will be moved to the configuration in later versions.


## Assumptions of the scheduled job

* Backups should only be attempted after 3 am.
* When not having created a backup on a server yet, retry every 30 minutes.

These assumptions can be changed in the settings of the scheduled job.


# Tutorials I found useful while creating TBo-CLI

* [this tutorial where most parts of the script originate from](https://neverpanic.de/blog/2022/01/25/backing-up-your-android-phone-with-borgbackup/)
* [this tutorial on how to parse command line arguments in bash](https://brianchildress.co/named-parameters-in-bash/)
* [termux cheat sheet](https://stephane-cheatsheets.readthedocs.io/en/latest/android/termux)


# How to contribute, and planned changes

If you find any bugs – unexpected errors, behaviour deviating from what is specified in the documentation – please open a [bug ticket](https://gitlab.com/blitter/tbo-cli/-/issues) (if there is no bugticket yet for this specific problem).

TBo-CLI is an open source project, so if there is anything you want to change you can clone the project and do with it whatever you want – but if you distribute your changes to others, you have to distribute your source code as well, as stated by the license.

If you feel that your changes may be useful to others – may it be source code or addition documentation –, feel free to open a merge request to this project :-)


## License

This project mainly depends on [borg](https://www.borgbackup.org/) (published under [BSD License](https://borgbackup.readthedocs.io/en/stable/authors.html#license) license) and the [termux app](https://github.com/termux/termux-app) (published under [GPLv3 only](https://github.com/termux/termux-app/blob/master/LICENSE.md) license). (Minor dependencies include some termux packages, including openssh, as stated above.) Therefore this project only depends on [FOSS](https://itsfoss.com/what-is-foss/) code.

This project is published under [Affero GPLv3 only](LICENSE) license.


## Things to test

When doing changes, please test that the following still works:

* the script is capable to send backups as configured
* the script is capable of pruning backups (as described in the [assumptions section](#assumptions-of-the-script-as-for-version-1))
* by default, the script does not perform a backup when on the same day to the same server a backup was already created
* by default, the script does not perform a backup when the battery status is not within the parameters as described in the [assumptions section](#assumptions-of-the-script-as-for-version-1)
* the `--BORGBACKUP_FORCE 1` argument works and overrides any circumstances normally prohibiting backups, regarding time, previous backups, and charging status
* consider running [shellcheck](https://www.shellcheck.net/). Document all cases where whitespace is not allowed.

## Introduced since the last major version (version 1)

* Documentation

## Upcoming breaking changes for version 2

Mandatory configuration:

* an option for backing up contacts (using `termux-contact-list`) (options has to be supplied in a configuration file)
* config files will be written in TOML
* adding separate config options for
    * loading status requirement
    * earliest and latest time for backups during the day, and time to pass until restart (by device)
* separate configuration for pruning times (yet to be decided whether by device or by server or one file in total)

Optional configuration:

* directory to put marker files in (default: users home directory). Argument in client's config.

Reaction to errorneous configuration:

* check for existence as first thing in every run: (and recommend )
    * secrets (nonempty)
    * hostname
    * matching client configuration
    * host configuration for given secrets


## Features that are not planned yet, but maybe later

* maybe termux-tasker instead of termux-job-scheduler?
* `--help` option that displays a useful overview
* packing the script as a termux [pkg](https://wiki.termux.com/wiki/Package_Management)
* a [manpage](https://www.howtogeek.com/682871/how-to-create-a-man-page-on-linux/)
* packing the script as an fdroid app
* maybe passing some/all parameters (configuration options) as command line arguments instead of file contents
* send real notification upon backup
* send real notifaction when not been able to make make backup for ~10h
