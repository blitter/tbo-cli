#!/data/data/com.termux/files/usr/bin/bash

# TODO
# * when to run "borg compact"? -> evtl eigener regelmäßiger Befehl, 1xwoche oder so
# * how to start and stop regular process
# * docs (richtiges public repo)
# * error wenn repo nicht existiert sinnvoll + wie repo erstellen!
# * subrepo for config
# * set up on my other phone to check that it worked

FORCE_MSG="Use \`--BORGBACKUP_FORCE 1\` to force a backup."


BORGBACKUP_FORCE=${BORGBACKUP_FORCE:-0}

#https://brianchildress.co/named-parameters-in-bash/
while [ $# -gt 0 ]; do
  if [[ $1 == *"--"* ]]; then
      param="${1/--/}"
      declare "${param}"="$2"
      # echo $1 $2 // Optional to see the parameter:value result
  fi
  shift
done

cleanup() {
  for notification in "${TERMUX_NOTIFICATIONS[@]}"; do
    termux-notification-remove "$notification"
  done
  termux-wake-unlock
}

##
# Send a notification to the user.
#
# Usage: echo "message" | notify persist identifier [options...]
#
# If persist is 0, the notification will be removed when the script exits.
# Otherwise, it will be kept (e.g. for warning or error messages).
#
# The identifier can be used to overwrite a previous notification with the same
# identifier. This can be useful for progress messages.
#
# Further options are those supported by termux-notification. The message must
# be passed on stdin.
notify() {
  local persist=$1
  shift
  local id="$1"
  shift
  local -a args=("--group" "${TERMUX_NOTIFICATION_ID}" "--id" "$id")

  if termux-notification "${args[@]}" "$@"; then
    if [ "$persist" -eq 0 ]; then
      TERMUX_NOTIFICATIONS+=("$id")
    fi
  fi
}

msg() {
  echo "***" "$@"
}

info() {
  msg "INFO:" "$@"
  termux-toast -s "$*"
}

warn() {
  msg "WARN:" "$@"
  echo "Warning:" "$@" | \
    notify 1 failure \
      --title "borgbackup" \
      --alert-once \
      --priority low
}

err() {
  msg "ERROR:" "$@"
  echo "Error:" "$@" | \
    notify 1 failure \
      --title "borgbackup" \
      --alert-once \
      --priority high
  exit 1
}

prepare() {
  PERCENTAGE=$(termux-battery-status | grep "percentage" | grep -E "[[:digit:]]*" --only-matching )

  #CHARGING is a substring of DISCHARGING
  #grep compares full line of string -> can't compare against ^CHARGING$
  #therefore comparing against "CHARGING"
  if ! ( [ "${BORGBACKUP_FORCE}" -eq 1 ] || \
    ( termux-battery-status | grep "status" | grep -qE "FULL" ) || \
    ( termux-battery-status | grep "status" | grep -qE "\"CHARGING\"" && [ "$PERCENTAGE" -ge 10 ] ) ||\
    [ "${PERCENTAGE}" -ge 80 ] \
  ); then

    warn "Won't perform backup as the phone is either <10% or not charging and <80%"
    echo "$FORCE_MSG"
    return 1
  fi
  if ! termux-wifi-connectioninfo | grep "supplicant_state" | grep -q "COMPLETED"; then
    warn "WiFi not connected, not performing backup"
    echo "$FORCE_MSG"
    return 1
  fi
}

backup() {
  local -a flags=()

  # enable interactive output
  if [ -t 0 ] && [ -t 1 ]; then
    flags+=('--stats' '--progress' '--list')
  fi

  BACKUP_DIRS=$(cat config/clients/"${HOSTNAME}") <<< readarray

  info "Starting uploading"
  ionice -c 3 \
    nice -n20 \
    borg create \
    --noatime \
    --compression='lz4' \
    --exclude-caches \
    "${flags[@]}" \
    "${TARGET}::${HOSTNAME}-{utcnow:%Y-%m-%dT%H:%M:%S}" \
    ${BACKUP_DIRS}
    # spaces in $BACKUP_DIRS are intentionally kept (for now)
    # local backup of other apps, i.e. signal
}


prune() {
  local -a flags=()

  # enable interactive output
  if [ -t 0 ] && [ -t 1 ]; then
    flags+=('--list')
  fi

  info "Pruning old backups..."
  borg prune \
    --prefix="${HOSTNAME}-" \
    --keep-within=14d \
    --keep-daily=31 \
    --keep-weekly=$((6 * 4)) \
    --keep-monthly=$(( 2 * 12 )) \
    --keep-yearly=$((10)) \
    "${flags[@]}" \
    "${TARGET}"
}


process_for_host() {
  local hostfile=$1
  info "Starting Backup for $1"
  source config/hosts/"$hostfile.sh" #https://stackoverflow.com/a/8352939
  TARGET="ssh://${USERNAME}@${HOST}:${PORT}${BASEDIR}/$HOSTNAME"

  export BORG_PASSCOMMAND="cat secrets/$hostfile" #todo automatically invoke
  declare -a TERMUX_NOTIFICATIONS=()
  TERMUX_NOTIFICATION_ID="borgbackup-$HOSTNAME"

  if ! ping -w 10 -c 3 "$HOST" >/dev/null; then
    warn "Failed to ping target $HOST"
    return 1
  fi

  # Run once per day, unless BORGBACKUP_FORCE=1
  MARKER_START=.borgbackup-"${hostfile}"
  MARKER_FILE=~/"${MARKER_START}-$(date +%Y-%m-%d)"
  if [ "${BORGBACKUP_FORCE}" -ne 1 ]; then
    if [ "$(date +%H)" -lt 3 ]; then
      echo "Backup not yet due, waiting..."
      echo "$FORCE_MSG"
      return 0
    elif [ -f "$MARKER_FILE" ]; then
      echo "Backup already ran today"
      echo "$FORCE_MSG"
      return 0
    fi
  fi

  rm -f ~/"${MARKER_START}"-*

  trap "cleanup" EXIT
  termux-wake-lock
  notify 0 progress \
    --alert-once \
    --ongoing \
    --priority low \
    --title "borgbackup" \
    --content "Running backup for $HOSTNAME"

  if ! backup; then
    err "Backup failed, aborting!"
    return 1
  fi

  touch "$MARKER_FILE" #only touch this file when success!

  if ! prune; then
    warn "Pruning failed. Continuing anyway."
    return 1
  fi
}


HOSTNAME=$(cat config/hostname)
PWD_DIR="secrets/*"

if ! prepare; then
  info "Server connectivity or charging status does not meet expectations, skipping backup."
  echo "$FORCE_MSG"
  exit 1
fi

for host in $PWD_DIR; do
   process_for_host $(basename "${host}")
done

#process_for_host("anna")
info "Backup finished successfully"
